#coding:utf-8
import http.server
import socketserver

port = 81
address = ("", port)

handler = http.server.SimpleHTTPRequestHandler
httpd = socketserver.TCPServer(address, handler)

print(f"Server start at PORT {port}")

httpd.serve_forever()
